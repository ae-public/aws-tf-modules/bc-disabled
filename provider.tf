provider "aws" {
  region = var.aws_region
}

provider "aws" {
  region = var.aws_region
  alias  = "this"
  assume_role {
    role_arn = "arn:aws:iam::${var.org_account_id}:role/${var.org_admin_role}"
  }
}

provider "aws" {
  region = var.aws_region
  alias  = "that"
  assume_role {
    role_arn = "arn:aws:iam::${var.org_account_id}:role/${var.org_admin_role}"
  }
}
