variable "org_name" {
  type    = string
  default = "BoxChamp"
}

variable "acc_name" {
  description = "Account Name"
  type        = string
  default = ""
}

variable "aws_region" {
  type    = string
  default = "vi or  "
}

variable "org_account_id" {
  type = string
  default = ""
}

variable "aws_organizations_account" {
  type = string
  default = ""
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "instance_type_family" {
  type    = string
  default = "t2"
}

variable "instance_types" {
  type    = string
  default = "t2.micro, t2.small, t3.nano"
}

variable "pub_subnets" {
  type = string
  default = ""
}

variable "prv_subnets" {
  type = string
  default = ""
}

variable "force_bucket_destruction" {
  type    = bool
  default = false
}

variable "aws_vpc_id" {
  type = string
  default = ""
}

variable "min_instances" {
  type    = string
  default = "2"
}

variable "max_instances" {
  type    = string
  default = "3"
}

variable "tags" {
  description = "Map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "ou_name" {
  description = "Name of OU"
  type        = string
  default = ""
}

variable "org_email_prefix" {
  description = "Organisations email prefix"
  type        = string
  default = ""
}

variable "org_email_suffix" {
  description = "Organisations email suffix"
  type        = string
  default = ""
}

variable "tf_bucket" {
  description = "Terraform State S3 Bucket Name"
  type        = string
  default     = "tf_bucket"
}

variable "tf_dynamodb_table" {
  description = "Terraform State Lockiing DDB Table"
  type        = string
  default     = "tf_dynamodb_table"
}

variable "org_parent_id" {
  description = "Parent to bind account to - either an OU or parent account"
  type        = string
  default = ""
}

variable "central_log_bucket" {
  type = string
  default = ""
}

variable "log_bucket_name" {
  type = string
  default = ""
}

variable "create_log_bucket" {
  type    = bool
  default = false
}

variable "create_central_log_bucket" {
  type    = bool
  default = false
}

variable "tfstate_prep" {
  type    = bool
  default = false
}

variable "org_admin_role" {
  type    = string
  default = "OrgAdminRole"
}

variable "min_pw_length" {
  type    = number
  default = 12
}

variable "pw_history" {
  type    = number
  default = 6
}

variable "max_pw_age" {
  type    = number
  default = 180
}

variable "allow_security_acc" {
  type    = bool
  default = true
}

variable "is_security_acc" {
  type    = bool
  default = false
}

variable "org_security_acc_id" {
  type    = string
  default = ""
}

variable "org_member_ids" {
  type    = list(string)
  default = ["undefined"]
}

variable "vpc_netblock" {
  type    = string
  default = "10.0.0.0/16"
}

variable "solution_stack_name" {
  type    = string
}

variable "ebe_settings" {
  type = list
}
