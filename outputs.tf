output "public_subnets" {
  value = []
}

output "private_subnets" {
  value = []
}

output "aws_ig" {
  value = ""
}

output "vpc_id" {
  value = ""
}

output "az_ids" {
  value = ""
}

output "az_names" {
  value = ""
}

output "pub_routing_table" {
  value = ""
}

output "prv_routing_table" {
  value = ""
}